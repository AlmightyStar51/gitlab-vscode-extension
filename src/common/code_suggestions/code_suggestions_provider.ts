import * as vscode from 'vscode';
import fetch from '../fetch_logged';
import { log } from '../log';
import { GITLAB_COM_URL } from '../constants';
import { GitLabPlatform, GitLabPlatformManager } from '../platform/gitlab_platform';
import { CodeSuggestionsTokenManager, CompletionToken } from './code_suggestions_token_manager';
import { CircuitBreaker } from './circuit_breaker';
import { Model, Telemetry, codeSuggestionsTelemetry } from './code_suggestions_telemetry';
import { GitLabProject } from '../platform/gitlab_project';
import {
  CodeSuggestionsStateManager,
  TemporaryCodeSuggestionsState,
} from './code_suggestions_state';
import {
  MODEL_GATEWAY_AI_ASSISTED_CODE_SUGGESTIONS_API_URL,
  GITLAB_AI_ASSISTED_CODE_SUGGESTIONS_API_PATH,
} from './constants';
import { prettyJson } from '../utils/json';
import { COMMAND_CODE_SUGGESTION_ACCEPTED } from './commands/code_suggestion_accepted';
import { PostRequest } from '../platform/web_ide';
import { getExtensionConfiguration } from '../utils/extension_configuration';
import { LegacyApiFallbackConfig } from './legacy_api_fallback_config';

export const CIRCUIT_BREAK_INTERVAL_MS = 10000;
export const MAX_ERRORS_BEFORE_CIRCUIT_BREAK = 4;

interface Choice {
  text: string;
  index: number;
  finish_reason: string;
}

interface CodeSuggestionsResponse {
  id: string;
  model: Model;
  object: string;
  created: number;
  choices: Choice[];
  usage: null;
}

interface CurrentFile {
  content_above_cursor: string;
  content_below_cursor: string;
  file_name: string;
}

export interface CodeSuggestionPrompt {
  current_file: CurrentFile;
  prompt_version: number;
  project_id?: number;
  project_path?: string;
  telemetry: Telemetry[];
}

const isSaasProject = (project: GitLabProject) => project.webUrl.startsWith(GITLAB_COM_URL);

export class CodeSuggestionsProvider implements vscode.InlineCompletionItemProvider {
  private server: string;

  private debouncedCall?: NodeJS.Timeout;

  private debounceTimeMs = 500;

  private noDebounce: boolean;

  private tokenManager: CodeSuggestionsTokenManager;

  private manager: GitLabPlatformManager;

  private stateManager: CodeSuggestionsStateManager;

  private legacyApiFallbackConfig: LegacyApiFallbackConfig;

  private circuitBreaker = new CircuitBreaker(
    MAX_ERRORS_BEFORE_CIRCUIT_BREAK,
    CIRCUIT_BREAK_INTERVAL_MS,
  );

  constructor({
    manager,
    stateManager,
    noDebounce = false,
  }: {
    manager: GitLabPlatformManager;
    stateManager: CodeSuggestionsStateManager;
    noDebounce?: boolean;
  }) {
    this.server = CodeSuggestionsProvider.#getServer();
    this.debouncedCall = undefined;
    this.noDebounce = noDebounce;
    this.tokenManager = new CodeSuggestionsTokenManager(manager);
    this.legacyApiFallbackConfig = new LegacyApiFallbackConfig();
    this.manager = manager;
    this.stateManager = stateManager;
  }

  static #getServer(): string {
    const serverUrl = new URL(MODEL_GATEWAY_AI_ASSISTED_CODE_SUGGESTIONS_API_URL);
    log.debug(`AI Assist: Using server: ${serverUrl.href}`);
    return serverUrl.href;
  }

  // TODO: Sanitize prompt to prevent exposing sensitive information
  // Issue https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/692
  static #getPrompt(
    document: vscode.TextDocument,
    position: vscode.Position,
    platform: GitLabPlatform,
  ): CodeSuggestionPrompt {
    const contentBeforeCursor = document.getText(
      new vscode.Range(0, 0, position.line, position.character),
    );

    const contentAfterCursor = document.getText(
      new vscode.Range(position.line, position.character, document.lineCount, 0),
    );

    const fileName = document.uri.path.split('/').pop() || '';

    const projectInfo = isSaasProject(platform.project)
      ? { project_id: platform.project.restId, project_path: platform.project.namespaceWithPath }
      : {};

    const payload = {
      prompt_version: 1,
      current_file: {
        file_name: fileName,
        content_above_cursor: contentBeforeCursor,
        content_below_cursor: contentAfterCursor,
      },
      ...projectInfo,
      telemetry: codeSuggestionsTelemetry.toArray(),
    };

    return payload;
  }

  async getCompletions({
    document,
    position,
    cancellationToken,
  }: {
    document: vscode.TextDocument;
    position: vscode.Position;
    cancellationToken: vscode.CancellationToken;
  }): Promise<vscode.InlineCompletionItem[]> {
    if (this.circuitBreaker.isBreaking()) {
      return [];
    }

    const platform = await this.manager.getForActiveProject(false);
    if (!platform) {
      log.warn(
        'AI Assist: could not obtain suggestions, there is no active project. Open GitLab project to continue',
      );
      this.stateManager.setTemporaryState(TemporaryCodeSuggestionsState.ERROR);
      return [];
    }

    const prompt = CodeSuggestionsProvider.#getPrompt(document, position, platform);

    // FIXME: when we start supporting SM, we need to get the token from the **platform**, now the project might not match the token
    // Also, passing the project to the API might get deprecated: https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/merge_requests/143#note_1419849871
    const token = await this.tokenManager.getToken();
    if (!token) {
      log.error('AI Assist: Could not fetch token');
      return [];
    }

    log.debug(
      `AI Assist: fetching completions ... (telemetry: ${prettyJson(
        codeSuggestionsTelemetry.toArray(),
        0,
      )})`,
    );

    let response: CodeSuggestionsResponse;
    let model: Model = { engine: '', name: '', lang: '' }; // Defaults for telemetry of failed requests

    const gitlabMonolithApiAvailable = (project: GitLabProject) =>
      !this.legacyApiFallbackConfig.shouldUseModelGateway() &&
      (getExtensionConfiguration().featureFlags.forceCodeSuggestionsViaMonolith ||
        isSaasProject(project));

    try {
      this.stateManager.setTemporaryState(TemporaryCodeSuggestionsState.LOADING);
      if (gitlabMonolithApiAvailable(platform.project)) {
        response = await this.fetchCompletionsFromGitLab(platform, token, prompt);
      } else {
        response = await this.fetchCompletions(platform, token, prompt);
      }

      this.stateManager.setTemporaryState(null);

      if (response.model !== undefined) model = response.model;

      this.circuitBreaker.success();

      // The previous counts were successfully sent...
      codeSuggestionsTelemetry.resetCounts();

      if (!cancellationToken.isCancellationRequested) {
        // Keep track of this request for next send..
        codeSuggestionsTelemetry.incRequestCount(model);
      } else {
        log.debug(
          'Code suggestions result is discarded because the completion request has been cancelled by the VS Code',
        );
        return [];
      }
    } catch (e) {
      log.error(`AI Assist: Error fetching completions: ${e.toString()}`);
      this.circuitBreaker.error();
      this.stateManager.setTemporaryState(TemporaryCodeSuggestionsState.ERROR);

      codeSuggestionsTelemetry.incRequestCount(model);
      codeSuggestionsTelemetry.incErrorCount(model);
      return [];
    }
    const choices = response.choices || [];

    log.debug(`AI Assist: got ${choices.length} completions`);

    // This command will be called when a suggestion is accepted
    const acceptedCommand: vscode.Command = {
      title: 'Code Suggestion Accepted',
      command: COMMAND_CODE_SUGGESTION_ACCEPTED,
      arguments: [model],
    };

    return choices.map(
      choice =>
        new vscode.InlineCompletionItem(
          choice.text,
          new vscode.Range(position, position),
          acceptedCommand,
        ),
    );
  }

  async provideInlineCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    context: vscode.InlineCompletionContext,
    cancellationToken: vscode.CancellationToken,
  ): Promise<vscode.InlineCompletionItem[]> {
    if (this.debouncedCall !== undefined) {
      clearTimeout(this.debouncedCall);
    }

    return new Promise(resolve => {
      //  In case of a hover, this will be triggered which is not desired as it calls for a new prediction
      if (context.triggerKind === vscode.InlineCompletionTriggerKind.Automatic) {
        if (this.noDebounce) {
          resolve(this.getCompletions({ document, position, cancellationToken }));
        } else {
          this.debouncedCall = setTimeout(() => {
            resolve(this.getCompletions({ document, position, cancellationToken }));
          }, this.debounceTimeMs);
        }
      }
    });
  }

  async fetchCompletions(
    platform: GitLabPlatform,
    token: CompletionToken,
    prompt: CodeSuggestionPrompt,
  ): Promise<CodeSuggestionsResponse> {
    log.debug(`AI Assist: fetching completions...`);

    const requestOptions = {
      method: 'POST',
      headers: {
        ...platform.getUserAgentHeader(),
        'X-Gitlab-Authentication-Type': 'oidc',
        Authorization: `Bearer ${token.access_token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(prompt),
    };

    const response = await fetch(this.server, requestOptions);

    await this.handleErrorReponse(response);

    const data = await response.json();
    return data;
  }

  async fetchCompletionsFromGitLab(
    platform: GitLabPlatform,
    token: CompletionToken,
    prompt: CodeSuggestionPrompt,
  ): Promise<CodeSuggestionsResponse> {
    log.debug(`AI Assist: fetching completions via monolith...`);
    const codeSuggestionRequest: PostRequest<CodeSuggestionsResponse> = {
      type: 'rest',
      method: 'POST',
      path: GITLAB_AI_ASSISTED_CODE_SUGGESTIONS_API_PATH,
      body: prompt,
      headers: {
        ...platform.getUserAgentHeader(),
        'X-Gitlab-Authentication-Type': 'oidc',
        'X-Gitlab-Oidc-Token': token.access_token,
      },
    };

    let response: CodeSuggestionsResponse;
    // Temporary handling for graduall rollout of new API, once API
    // is globaly available all error handling should be removed
    // https://gitlab.com/gitlab-org/gitlab/-/issues/419809
    try {
      response = await platform.fetchFromApi(codeSuggestionRequest);
    } catch (e) {
      if ('status' in e && e.status === 403) {
        this.legacyApiFallbackConfig.fallbackToModelGateway();
        response = await this.fetchCompletions(platform, token, prompt);
      } else {
        throw e;
      }
    }
    return response;
  }

  private async handleErrorReponse(response: Response) {
    if (!response.ok) {
      const body = await response.text().catch(() => undefined);
      throw new Error(
        `Fetching code suggestions from ${response.url} failed for server ${this.server}. Body: ${body}`,
      );
    }
  }
}
